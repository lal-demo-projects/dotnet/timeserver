﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Threading.Tasks;

namespace TimeServerConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = GetTime().Result;
            if (result.HasValue)
            {
                Console.WriteLine("Network UTC Time - " + result.Value.ToLongDateString() + " " + result.Value.ToLongTimeString());
            }
            else
            {
                Console.WriteLine("Unable to read network time. Please try again");
            }
            Console.ReadLine();
        }

        public static async Task<DateTime?> GetTime()
        {
            List<ServerItem> servers = await GetTimeServer();
            servers = GetRandomServer(servers);
            return await GetTime(servers);
        }

        public static async Task<DateTime?> GetTime(List<ServerItem> servers)
        {
            DateTime? Result = null;
            INetworkTimeService ntpService = null;
            INetworkTimeService nistService = null;
            foreach (var server in servers)
            {
                if (server.IsNTP)
                {
                    if (ntpService == null) ntpService = new NTPService();
                    Result = await ntpService.GetNetworkUTCTimeAsync(server.URL);
                }
                else if (server.IsNIST)
                {
                    if (nistService == null) nistService = new NISTService();
                    Result = await nistService.GetNetworkUTCTimeAsync(server.URL);
                }
                if (Result.HasValue) break;
            }
            return Result;
        }

        public static async Task<List<ServerItem>> GetTimeServer()
        {
            string serverStr = string.Empty;
            string localFilePath = AppContext.BaseDirectory + "TimeServers.json";
            string dataURL = "https://lal-public-pages.gitlab.io/jsondata/TimeServers/TimeServers.json";
            if (File.Exists(localFilePath))
            {
                serverStr = File.ReadAllText(localFilePath);
            }
            else
            {
                using (WebClient client = new WebClient())
                {
                    serverStr = await client.DownloadStringTaskAsync(dataURL);
                }
                File.WriteAllText(localFilePath, serverStr);
            }
            List<ServerItem> Result = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ServerItem>>(serverStr);
            return Result;
        }

        public static List<ServerItem> GetRandomServer(List<ServerItem> serverList)
        {
            int selectCount = 5;
            List<ServerItem> Result = new List<ServerItem>();
            Random rnd = new Random();
            for (int i = 0; i < selectCount; i++)
            {
                Result.Add(serverList[rnd.Next(serverList.Count)]);
            }
            return Result;
        }
    }
}
