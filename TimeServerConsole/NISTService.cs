﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace TimeServerConsole
{
    public class NISTService : INetworkTimeService
    {
        public async Task<DateTime?> GetNetworkUTCTimeAsync(string server)
        {
            DateTime? result = null;
            int port = 13; // NIST Server port

            try
            {
                result = await GetNISTServerUTCTimeAsync(server, port);
            }
            catch { }

            return result;
        }

        private async Task<DateTime?> GetNISTServerUTCTimeAsync(string server, int port)
        {
            DateTime? result = null;
            // Connect to the server and get the response
            var client = new System.Net.Sockets.TcpClient();
            string serverResponse = string.Empty;
            client.ReceiveTimeout = 3000;
            await client.ConnectAsync(server, port);
            using (var reader = new StreamReader(client.GetStream()))
            {
                serverResponse = await reader.ReadToEndAsync();
            }

            // If a response was received
            if (!string.IsNullOrEmpty(serverResponse))
            {
                // Split the response string ("55596 11-02-14 13:54:11 00 0 0 478.1 UTC(NIST) *")
                string[] tokens = serverResponse.Split(' ');

                // Check the number of tokens
                if (tokens.Length >= 6)
                {
                    // Check the health status
                    string health = tokens[5];
                    if (health == "0")
                    {
                        // Get date and time parts from the server response
                        string[] dateParts = tokens[1].Split('-');
                        string[] timeParts = tokens[2].Split(':');

                        // Create a DateTime instance
                        DateTime utcDateTime = new DateTime(
                            Convert.ToInt32(dateParts[0]) + 2000,
                            Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[2]),
                            Convert.ToInt32(timeParts[0]), Convert.ToInt32(timeParts[1]),
                            Convert.ToInt32(timeParts[2]), DateTimeKind.Utc);

                        result = utcDateTime;
                        // Response successfully received
                    }
                }
            }
            return result;
        }
    }
}
