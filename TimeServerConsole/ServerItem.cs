﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeServerConsole
{
    public class ServerItem
    {
        public string URL { get; set; }
        public int? Stratum { get; set; }
        public string Source { get; set; }
        public string Details { get; set; }
        public bool IsNTP { get; set; }
        public bool IsNIST { get; set; }
        public bool IsWorking { get; set; }
    }
}
