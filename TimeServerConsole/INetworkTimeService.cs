﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeServerConsole
{
    public interface INetworkTimeService
    {
        Task<DateTime?> GetNetworkUTCTimeAsync(string server);
    }
}
